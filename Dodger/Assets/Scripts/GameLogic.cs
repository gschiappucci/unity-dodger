﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour {

    #region Singleton
    public static GameLogic instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    public static GameState state = GameState.Playing;
    //Gets total height and width of the play area
    public static float screenHeight;
    public static float screenWidth;
    
    public GameObject asteroid;
    public GameObject star;
    public Text scoreText;

    private int score;
    private float asteroidTimer;
    private float starTimer;
    private int starAmount = 64;    

	// Use this for initialization
	void Start () {
        //Gets total height and width of the play area
        screenHeight = Camera.main.orthographicSize * 2.0f;
        screenWidth = screenHeight * Screen.width / Screen.height;
        ResetAsteroidTimer();
        ResetStarTimer();

        switch (state)
        {
            case GameState.Playing:
                InitializeStars();
                break;
            case GameState.Over:
                break;
        }
    }
	
	// Update is called once per frame
	void Update () {
        switch (state)
        {
            case GameState.Playing:
                SpawnStars();
                SpawnAsteroids();
                PrintScore();
                break;
            case GameState.Over:
                SpawnStars();
                break;
        }
	}

    private void PrintScore()
    {
        scoreText.text = "Score: " + score.ToString();
    }

    private void SpawnAsteroids()
    {
        asteroidTimer -= Time.deltaTime;
        if (asteroidTimer <= 0)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-screenWidth / 2, screenWidth / 2), (screenHeight /2) + 1, 0);
            Instantiate(asteroid, spawnPosition, transform.rotation, transform.Find("Asteroids").transform);
            ResetAsteroidTimer();
        }
    }

    private void SpawnStars()
    {
        starTimer -= Time.deltaTime;
        if (starTimer <= 0)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-screenWidth / 2, screenWidth / 2), (screenHeight / 2), 0);
            Instantiate(star, spawnPosition, transform.rotation, transform.Find("Stars").transform);
            ResetStarTimer();
        }
    }

    private void InitializeStars()
    {
        for (int i = 0; i < starAmount; i++)
        {
            Vector3 starPosition = new Vector3(Random.Range(-screenWidth / 2, screenWidth / 2), Random.Range(-screenHeight / 2, screenHeight / 2), 0);
            Instantiate(star, starPosition, transform.rotation, transform.Find("Stars").transform);
        }
    }

    private void ResetAsteroidTimer()
    {
        asteroidTimer = 0.1f;
    }

    private void ResetStarTimer()
    {
        starTimer = 2f;
    }

    #region Public Methods
    public void IncreaseScore(int points)
    {
        score += points;
    }

    public void ResetScore()
    {
        score = 0;
    }

    public void GameOver()
    {
        state = GameState.Over;
    }

    public GameState GetState()
    {
        return state;
    }
    #endregion
}

public enum GameState
{
    Menu,
    Playing,
    Over
}
