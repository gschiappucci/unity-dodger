﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {
    [SerializeField]
    private float speed = 6f;

	// Update is called once per frame
	void Update () {
        MoveAsteroid();
        DestroyAsteroidOffCamera();
    }

    private void MoveAsteroid()
    {
        Vector3 newPosition = transform.position;
        newPosition.y -= speed * Time.deltaTime;
        transform.position = newPosition;
    }

    private void DestroyAsteroidOffCamera()
    {
        if (transform.position.y < -6)
        {
            Destroy(gameObject);
        }
    }
}
