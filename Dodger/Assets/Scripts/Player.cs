﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private Renderer playerRenderer;
    private GameLogic logic;

    // Use this for initialization
    void Start () {
        logic = GameLogic.instance; //Calls GameLogic Singleton
        logic.ResetScore();
        playerRenderer = GetComponent<Renderer>();
    }
	
	// Update is called once per frame
	void Update () {
        //Adds points
        switch (logic.GetState())
        {
            case GameState.Playing:
                logic.IncreaseScore(1);
                break;
            case GameState.Over:
                break;
        }

        MovePlayer();
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
        logic.GameOver();
        Destroy(gameObject);
    }

    private void MovePlayer()
    {
        //Get mouse position related to World Points
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0;

        //Clamps player to the screen
        mousePosition.x = Mathf.Clamp(mousePosition.x, (-GameLogic.screenWidth / 2) + (playerRenderer.bounds.size.x / 2), (GameLogic.screenWidth / 2) - (playerRenderer.bounds.size.x / 2));
        mousePosition.y = Mathf.Clamp(mousePosition.y, (-GameLogic.screenHeight / 2) + (playerRenderer.bounds.size.y / 2), (GameLogic.screenHeight / 2) - (playerRenderer.bounds.size.y / 2));

        //Sets position of the player to clamped mouse position
        transform.position = mousePosition;
    }
}
