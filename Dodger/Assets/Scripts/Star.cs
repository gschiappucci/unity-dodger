﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour {
    [SerializeField]
    private float speed = 0.1f;

	// Update is called once per frame
	void Update () {
        MoveStar();
        DestroyStarOffCamera();
    }

    private void MoveStar()
    {
        Vector3 newPosition = transform.position;
        newPosition.y -= speed * Time.deltaTime;
        transform.position = newPosition;
    }

    private void DestroyStarOffCamera()
    {
        if (transform.position.y < -5.5)
        {
            Destroy(gameObject);
        }
    }
}
